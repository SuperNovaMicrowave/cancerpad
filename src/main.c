#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curses.h>

char VERSION[] = "4.2.0";

char filename[255] = "untitled.txt";

int width, height;
int textfield_width, textfield_height;
int32_t cursor_pos_x, cursor_pos_y;

void title_bar();
void line_numbers();
void status_bar();

int main(int argc, char *argv[])
{
    if (argc == 2) if (strcmp(argv[1], "-v") == 0)
    {
        printf("version: %s", VERSION);
        return EXIT_SUCCESS;
    }
    else
    {
        strcpy(filename, argv[1]);
    }
    

    initscr();
    cbreak();
    noecho();
    keypad(stdscr, TRUE);
    nodelay(stdscr, TRUE);

    getmaxyx(stdscr, height, width);

    textfield_width = width - 2;
    textfield_height = height - 2;

    cursor_pos_x = 0;
    cursor_pos_y = 0;
    move(cursor_pos_y, cursor_pos_x);

    bool run = true;
    while (run)
    {
        int user_input;
        if ((user_input = getch()) == ERR)
        {
            /* no input */
        } else
        {
            switch (user_input)
            {
            case KEY_UP:
                if(cursor_pos_y > 0) cursor_pos_y--;
                break;
            case KEY_DOWN:
                if(cursor_pos_y < textfield_height-1) cursor_pos_y++;
                break;
            case KEY_LEFT:
                if(cursor_pos_x > 0) cursor_pos_x--;
                else if(cursor_pos_y > 0) /* left edge wraparound */
                {
                    cursor_pos_y--;
                    cursor_pos_x = textfield_width-1;
                }
                break;
            case KEY_RIGHT:
                if(cursor_pos_x < textfield_width-1) cursor_pos_x++;
                else if(cursor_pos_y < textfield_height-1) /* right edge wraparound */
                {
                    cursor_pos_y++;
                    cursor_pos_x = 0;
                }
                break;
            case KEY_F(10):
                run = false;
                break;
            default:
                addch(user_input);
                if(cursor_pos_x < textfield_width-1) cursor_pos_x++;
                else if(cursor_pos_y < textfield_height-1) /* left edge wraparound */
                {
                    cursor_pos_y++;
                    cursor_pos_x = 0;
                }
                
                break;
            }
        }
        
        attron(A_STANDOUT);
        title_bar();
        line_numbers();
        status_bar();
        attroff(A_STANDOUT);
        
        move(cursor_pos_y+1, cursor_pos_x+2);
        wrefresh(stdscr);
    }

    endwin();
    return EXIT_SUCCESS;
}

void title_bar()
{
    int i;
    for (i = 0; i < width-1; i++) /* background */
    {
        move(0, i);
        addch(' ');
    }
    for (i = 0; i < width-1; i++) /* content */
    {
        move(0, i);
        if(i == 0)
        {
            addstr("Cancerpad");
        }
        if(i == width - strlen(filename) - 1)
        {
            addstr(filename);
        }
    }
}

void line_numbers()
{
    int i;
    for (i = 0; i < height-2; i++) /* background */
    {
        move(i+1, 0);
        addch(' ');
        move(i+1, 1);
        addch(' ');
    }
    for (i = 0; i < height-2; i++) /* numbers */
    {
        char str[2];
        sprintf(str, "%d", i);

        if (i+1 < 11) move(i+1, 1);
        else move(i+1, 0);
        addstr(str);
    }
}

void status_bar()
{
    int i;
    for (i = 0; i < width; i++) /* background */
    {
        move(height-1, i);
        addch(' ');
    }
    for (i = 0; i < width; i++) /* content */
    {
        move(height-1, i);
        if (i == 0)
        {
            char x_str[8];
            sprintf(x_str, "C:%d", cursor_pos_x);
            addstr(x_str);
        }
        
        if (i == 12)
        {
            char y_str[8];
            sprintf(y_str, "L:%d", cursor_pos_y);
            addstr(y_str);
        }
        if (i == (width - 11))
        {
            addstr("F10 = EXIT");
        }
        
    }
}